/**
 * Router Class
 */
(function(GLOBAL, $LAB){


    function Router ( routes ) {
        this.views = {};
        this.models = {};
        this.routes = routes;
        this.getRoute();
        this.changeRoute();
    }

    Router.prototype.getRoute = function getCurrentRouting () {
        this.currentRoute = GLOBAL.location.hash.replace( '#', '' );
        console.log(this.currentRoute);
    };

    Router.prototype.changeRoute = function changeToRoute () {
        if ( this.routes.hasOwnProperty( this.currentRoute ) ) {
            this.routes[this.currentRoute].call(this);
        } else {
            // Change default route to dynamic set
            this.routes['default']();
            // GLOBAL.location.hash = 'default';
        }
    };

    Router.prototype.setRouter = function routerSetter ( newRoute ) {
        GLOBAL.location.hash = newRoute;
        this.currentRoute = newRoute;
        this.changeRoute();
    };

    Router.prototype.loadResource = function resourceLoading ( params ) {
        // scriptPath, type, name, namespace
        var router = this;
        $LAB.script( params.scriptPath + '.js' ).wait( function postLoadCallback () {
            router[params.namespace] = router[params.namespace] || {};
            router[params.namespace][params.name] = new GLOBAL[params.namespace][params.name + params.type]();

        });
                // init resource
    };

    GLOBAL.Router = Router;

})(window, $LAB);