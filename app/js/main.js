/* MVC PATH - SAM EXPLANATION
Router, Views, Templates, Models, DB - separate files!

    Router {Class}
* #login
* #register
* #viewDocuments
 -> goes to #Default if anything different that the other 3 is called

    Views - renders Template based on the data from the Model
-login
-register
-viewDocuments

    Templates
*login
*register
*viewDocuments

    Model
#user -> login/register/logout
#documents -> add/remove/edit/view

    DB

Folder Structure
    App
        view
            login
            register
            viewDocuments
        models
            user
            documents
        templates
            login
            register
            viewDocuments
        main.js (Router)

Entities (When we have a GROUP OF the same ENTITITES we have a COLLECTION)

*/

/**
 * Master application
 */
 window.Env = window.Env || {};

(function(GLOBAL){

    var App = {},
        routes;

    GLOBAL.Env.baseUrl = 'app/';
    GLOBAL.Env.jsUrl = GLOBAL.Env.baseUrl + 'js/';
    GLOBAL.Env.vendorUrl = GLOBAL.Env.jsUrl + 'vendor/';
    GLOBAL.Env.viewsUrl = GLOBAL.Env.baseUrl + 'views/';
    GLOBAL.Env.modelsUrl = GLOBAL.Env.baseUrl + 'models/';
    GLOBAL.Env.templatesUrl = GLOBAL.Env.baseUrl + 'templates/';

    App.init = function initializeApplication() {
        App.router = new Router(routes);
    };

    $LAB
    .setOptions({AlwaysPreserverOrder:true})
    .script([
        GLOBAL.Env.jsUrl + 'plugins.js',
        GLOBAL.Env.jsUrl + 'router.js',
        GLOBAL.Env.vendorUrl + 'alertify.min.js',
        GLOBAL.Env.vendorUrl + 'sdb_manager.js',
        GLOBAL.Env.vendorUrl + 'underscore.js'
    ])
    .wait(App.init);

    var routes = {
        'login': function loginRoute (argument) {
            // body...
            var router = this;
            console.log('login route');
            if ( router.views.login ) {
                // To Do
                // router.views.login.render();
            } else {
                router.loadResource({
                    scriptPath: GLOBAL.Env.viewsUrl + 'login-view',
                    type: 'View',
                    name: 'login',
                    namespace: 'views'
                });
            }
        },

        'register': function registerRoute (argument) {
            // body...
            console.log('register route');
        },

        'viewDocuments': function viewDocumentsRoute (argument) {
            // body...
            console.log('viewDocuments route');
        },

        'default': function viewDefaultRoute (argument) {
            // body...
            console.log('default route');
        },

    };

    // remove this after dev
    GLOBAL.App = App;
    /**
     * Document ready
     */
    $(function() {


    });

})(window);